package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    int getSize();

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}