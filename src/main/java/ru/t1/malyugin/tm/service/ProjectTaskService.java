package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IProjectTaskService;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.ProjectIdEmptyException;
import ru.t1.malyugin.tm.exception.field.TaskIdEmptyException;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (projectRepository.findOneById(projectId.trim()) == null) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId.trim());
        return task;
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (projectRepository.findOneById(projectId.trim()) == null) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId.trim());
        for (final Task task : tasks) taskRepository.remove(task);
        return projectRepository.removeById(projectId.trim());
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (projectRepository.findOneById(projectId.trim()) == null) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}