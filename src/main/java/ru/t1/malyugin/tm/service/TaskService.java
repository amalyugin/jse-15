package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.DescriptionEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        if (StringUtils.isBlank(description)) throw new DescriptionEmptyException();
        return taskRepository.create(name.trim(), description.trim());
    }

    @Override
    public Task create(final String name) {
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        return taskRepository.create(name.trim());
    }

    @Override
    public Task findOneById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return taskRepository.findOneById(id.trim());
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return taskRepository.removeById(id.trim());
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Task> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId.trim());
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = findOneById(id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        task.setDescription(description.trim());
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final Task task = findOneById(id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}