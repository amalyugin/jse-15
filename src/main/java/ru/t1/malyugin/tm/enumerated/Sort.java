package ru.t1.malyugin.tm.enumerated;

import ru.t1.malyugin.tm.comparator.CreatedComparator;
import ru.t1.malyugin.tm.comparator.NameComparator;
import ru.t1.malyugin.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    private final String displayName;

    private final Comparator<?> comparator;

    Sort(final String displayName, final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

    public static String renderValuesList() {
        StringBuilder result = new StringBuilder();
        final int size = Sort.values().length;
        for (int i = 0; i < size; i++) {
            result.append(String.format("%d - %s, ", i, Sort.values()[i].displayName));
        }
        return result.toString();
    }

    public static Sort getSortByIndex(final Integer index) {
        if (index == null || index < 0 || index >= Sort.values().length) return null;
        return Sort.values()[index];
    }

}